package com.ystu.soap.services.chat;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Parser;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.*;
import java.text.NumberFormat;

@Path("/file")
public class FileService {

    @Context ServletContext sc;
    private DBInterface dbi = new DBInterface();

    @Context
    UriInfo uri;

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public synchronized Response uploadFile(
            @CookieParam("SESSIONID")Long sessionid,
            @FormDataParam("file")InputStream fileInputString,
            @FormDataParam("file")FormDataContentDisposition fileInputDetails
            )
    {
        String folder = sc.getRealPath("\\resources\\files");
        String host = uri.getBaseUri().getHost();
        String port = String.valueOf(uri.getBaseUri().getPort());
        String filename = fileInputDetails.getFileName();
        Long fileId = dbi.saveFilename(filename);
        String message = "<a class='filename' href='http://"+host+":"+port+"/api/file/download/"+String.valueOf(fileId)+"'>";

        try {
            OutputStream out = new FileOutputStream(new File(folder+"\\"+fileId.toString()));
            byte[] buffer = new byte[1024];
            int bytes = 0;
            while ((bytes=fileInputString.read(buffer))!= -1) {
                out.write(buffer,0,bytes);
            }
            out.flush();
            out.close();
            message+=filename+"</a>";
            dbi.addMessage(sessionid,message);
            return Response.status(200).build();
        } catch (IOException e) {
            System.out.println("Unable to save file: "+folder+filename);
            return Response.status(403).build();
        }
    }

    @GET
    @Path("/download/{fileid}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadFilebyPath(@PathParam("fileid") Long fileId) {
        return download(fileId);
    }

    private Response download(Long fileId) {
        String folder = sc.getRealPath("\\resources\\files");
        String fileLocation = folder +"\\"+fileId.toString();
        Response response = null;

        // Retrieve the file
        File file = new File(fileLocation);
        if (file.exists()) {
            String filename = dbi.getFilename(fileId);
            Response.ResponseBuilder builder = Response.ok(file);
            builder.header("Content-Disposition", "attachment; filename=" + filename);
            response = builder.build();
        } else {
            response = Response.status(404).
                    entity("FILE NOT FOUND: " + fileLocation).
                    type("text/plain").
                    build();
        }

        return response;
    }
}
