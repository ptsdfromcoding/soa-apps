package com.ystu.soap.services.chat.db.entity;

import javax.persistence.*;
import java.io.File;

@Entity(name="Filedata")
public class Filedata {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long Id;

    @Column(name="filename")
    private String Filename;

    Filedata(){}

    public Filedata(String filename)
    {
        this.Filename = filename;
    }

    public String getFilename() {
        return Filename;
    }

    public void setFilename(String filename) {
        Filename = filename;
    }

    public Long getId() {
        return Id;
    }
}
