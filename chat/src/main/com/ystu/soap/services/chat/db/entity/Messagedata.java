package com.ystu.soap.services.chat.db.entity;

import javax.persistence.*;

@Entity(name="Messagedata")
public class Messagedata {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long Id;

    @Column(name="userid")
    private Long UserId;

    @Column(name="text")
    private String Text;

    public Messagedata(){}

    public Messagedata(Long userId, String text)
    {
        this.UserId = userId;
        this.Text = text;
    }

    public Messagedata(String text)
    {
        this.UserId = 1L;
        this.Text = text;
    }

    public String getText() {return Text;}
    public Long getUserId() {return UserId;}
    public Long getId() {return Id;}

    public void setText(String text)
    {
        this.Text = text;
    }
}
