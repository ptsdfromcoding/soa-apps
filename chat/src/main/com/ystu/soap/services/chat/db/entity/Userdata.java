package com.ystu.soap.services.chat.db.entity;

import javax.persistence.*;

@Entity(name="Userdata")
public class Userdata {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long Id;

    @Column(name="authtoken")
    private int AuthToken;

    @Column(name="username")
    private String Username;

    public Userdata(){}

    public Userdata(String username, String password)
    {
        this.Username = username;
        this.AuthToken = password.hashCode();
    }

    public Userdata(String username, int authToken)
    {
        this.Username = username;
        this.AuthToken = authToken;
    }

    public String getUsername() {return Username;}
    public int getAuthToken() {return AuthToken;}
    public Long getId() {return Id;}
}
