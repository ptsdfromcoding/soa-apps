package com.ystu.soap.services.chat.db.entity;

import javax.persistence.*;

@Entity(name="Sessiondata")
public class Sessiondata {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long Id;

    @Column(name="username")
    private String Username;

    @Column(name="userid")
    private Long UserId;

    @Column(name="sessiontoken")
    private Long SessionToken;

    public Sessiondata(){}

    public Sessiondata(String Username, Long UserId, Long SessionToken)
    {
        this.Username = Username;
        this.UserId = UserId;
        this.SessionToken = SessionToken;
    }


    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public Long getUserId() {
        return UserId;
    }

    public void setUserId(Long userId) {
        UserId = userId;
    }

    public Long getSessionToken() {
        return SessionToken;
    }

    public void setSessionToken(Long sessionToken) {
        SessionToken = sessionToken;
    }
}
