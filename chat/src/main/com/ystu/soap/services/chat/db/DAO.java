package com.ystu.soap.services.chat.db;


import com.sun.jersey.spi.resource.Singleton;
import com.ystu.soap.services.chat.data.User;
import com.ystu.soap.services.chat.db.entity.*;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Singleton
public class DAO {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("persist-unit");

    //fixes for no good reason
    public static <T> T getSingleResult(TypedQuery<T> query){
        List<T> list = query.getResultList();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    //File methods

    public String getFilename(Long fid)
    {
        EntityManager em = emf.createEntityManager();

        Filedata fd = null;

        fd = em.find(Filedata.class, fid);

        return fd.getFilename();
    }

    public Long saveFilename(Filedata fd)
    {
        EntityManager em = emf.createEntityManager();

        EntityTransaction mt = em.getTransaction();

        Boolean check = false;

            try{
                mt.begin();
                em.persist(fd);
                mt.commit();
                check = true;
            } catch (Exception e){
                System.out.println("Error: "+ e.getMessage());
                mt.rollback();
            }
            finally {
                em.close();
            }
            return fd.getId();
    }

    //Auth methods

    public Userdata findUser(Long Id){
        EntityManager em = emf.createEntityManager();

        Userdata ud = null;

        ud = em.find(Userdata.class, Id);

        return ud;
    }

    public Userdata findUser(String username){
        EntityManager em = emf.createEntityManager();

        Userdata ud = null;

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Userdata.class);
        Root<Userdata> from = cq.from(Userdata.class);

        cq.select(from).where(cb.equal(from.get("Username"),username));
        TypedQuery<Userdata> query = em.createQuery(cq);

        ud = getSingleResult(query);

        return ud;
    }

    public Sessiondata findSessionByToken(Long sessiontoken) {
        EntityManager em = emf.createEntityManager();

        Sessiondata sd = null;

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Sessiondata.class);
        Root<Sessiondata> from = cq.from(Sessiondata.class);

        cq.select(from).where(cb.equal(from.get("SessionToken"),sessiontoken));
        TypedQuery<Sessiondata> query = em.createQuery(cq);
        sd = getSingleResult(query);

        return sd;
    }

    public Long login(Long userid, int authtoken){
        EntityManager em = emf.createEntityManager();

        Userdata ud = null;
        Sessiondata sd = null;
        Long sessionID = ThreadLocalRandom.current().nextLong(1,9223372036854775807L);

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Userdata> criteria = builder.createQuery(Userdata.class);
        Root<Userdata> from = criteria.from(Userdata.class);
        criteria.select(from).where(builder.and(builder.equal(from.get("Id"), userid), builder.equal(from.get("AuthToken"), authtoken)));
        TypedQuery<Userdata> typed = em.createQuery(criteria);
        ud = getSingleResult(typed);
        if (ud!=null) {
            EntityTransaction mt = em.getTransaction();

            try {
                mt.begin();
                sd = new Sessiondata(ud.getUsername(), ud.getId(), sessionID);
                em.persist(sd);
                mt.commit();
                return sd.getSessionToken();
            } catch (final Exception nre) {
                mt.rollback();
                return null;
            } finally {
                em.close();
            }
        } else { return null; }
    }

    public Long login(String username, int authtoken){
        EntityManager em = emf.createEntityManager();

        Userdata ud = null;
        Sessiondata sd = null;
        Long sessionID = ThreadLocalRandom.current().nextLong(1,9223372036854775807L);

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Userdata> criteria = builder.createQuery(Userdata.class);
        Root<Userdata> from = criteria.from(Userdata.class);
        criteria.select(from).where(builder.and(builder.equal(from.get("Username"), username), builder.equal(from.get("AuthToken"), authtoken)));
        TypedQuery<Userdata> typed = em.createQuery(criteria);
        ud = getSingleResult(typed);
        if (ud!=null) {
            EntityTransaction mt = em.getTransaction();
            try {
                mt.begin();
                sd = new Sessiondata(ud.getUsername(), ud.getId(), sessionID);
                em.merge(sd);
                mt.commit();
                return sd.getSessionToken();
            } catch (final Exception nre) {
                mt.rollback();
                return null;
            } finally {
                em.close();
            }
        } else { return null; }
    }

    public Boolean createUser(Userdata ud){

        EntityManager em = emf.createEntityManager();

        EntityTransaction mt = em.getTransaction();

        Boolean check = false;

        if (findUser(ud.getUsername()) == null) {
            try{
                mt.begin();
                em.persist(ud);
                mt.commit();
                check = true;
            } catch (Exception e){
                System.out.println("Error: "+ e.getMessage());
                mt.rollback();
            }
            finally {
                em.close();
            }
            return check;
        }
        else {
            em.close();
            return check;
        }
    }

    public Long getRight(Long sessionToken) {
        Sessiondata sd = findSessionByToken(sessionToken);

        if (sd==null) return null;
        else return sd.getUserId();
    }

    public List<User> getUserlist() {
        EntityManager em = emf.createEntityManager();

        List<User> ul = new ArrayList<User>();
        List<Userdata> ud = null;

        EntityTransaction mt = em.getTransaction();

        try{
            mt.begin();
            ud = em.createQuery("SELECT m FROM Userdata m").getResultList();
            mt.commit();
        } catch (Exception e) {
            System.out.println("Error: "+ e.getMessage());
        } finally {
            em.close();
        }

        if (ud == null) {System.out.println("No users.");}
        else {

            for (Userdata u: ud) {
                User temp = new User(u.getId(),u.getUsername());
                ul.add(temp);
            }
        }

        return ul;
    }

    public Boolean logout(Long sessionid) {
        EntityManager em = emf.createEntityManager();

        EntityTransaction mt = em.getTransaction();

        Sessiondata sd = findSessionByToken(sessionid);

        if (sd!=null){
            Boolean check = false;
            try {
                mt.begin();
                em.remove(sd);
                mt.commit();
                check = true;
            } catch (Exception e) {
                mt.rollback();
            } finally {
                em.close();
                return check;
            }
        }
        else return false;
    }

    //Message methods

    public Boolean addMessage(Messagedata md){
        EntityManager em = emf.createEntityManager();
        EntityTransaction mt = em.getTransaction();
        Boolean check = false;

        try{
            mt.begin();
            em.persist(md);
            mt.commit();
            check = true;
        } catch (Exception e){
            System.out.println("Error: "+ e.getMessage());
            mt.rollback();
        }
        finally {
            em.close();
            return check;
        }
    }

    public List<Messagedata> getAllMessages() {
        EntityManager em = emf.createEntityManager();

        List<Messagedata> md = null;

        EntityTransaction mt = em.getTransaction();

        try{
            mt.begin();
            md = em.createQuery("SELECT m FROM Messagedata m").getResultList();
            mt.commit();
        } catch (Exception e) {
            System.out.println("Error: "+ e.getMessage());
        } finally {
            em.close();
        }

        if (md == null) {System.out.println("No messages");}

        return md;
    }

    public Messagedata getMessage(Long msgId) {

        EntityManager em = emf.createEntityManager();
        Messagedata md = emf.createEntityManager().find(Messagedata.class,msgId);
        em.close();

        return md;
    }

    public Boolean editMessage(Long userid, Long msgId, String newmessage) {
        EntityManager em = emf.createEntityManager();
        Messagedata md = null;
        EntityTransaction mt = em.getTransaction();

        md = em.find(Messagedata.class,msgId);

        if (md.getUserId()==userid) {
            Boolean check = false;
            try {
                mt.begin();
                md.setText(newmessage);
                em.merge(md);
                mt.commit();
                check = true;
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
                mt.rollback();
            } finally {
                em.close();
                return check;
            }
        } else return false;
    }

    public Boolean deleteMessage(Long userid, Long msgId) {
        EntityManager em = emf.createEntityManager();
        Messagedata md = null;
        EntityTransaction mt = em.getTransaction();

        md = em.find(Messagedata.class,msgId);

        if (md!=null) {
            Boolean check = false;
            try {
                mt.begin();
                em.remove(md);
                mt.commit();
                check = true;
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
                mt.rollback();
            } finally {
                em.close();
                return check;
            }
        }
        else return false;
    }
}
