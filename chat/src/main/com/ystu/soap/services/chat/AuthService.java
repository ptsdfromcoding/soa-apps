package com.ystu.soap.services.chat;

import com.sun.org.apache.xpath.internal.operations.Bool;
import com.ystu.soap.services.chat.data.User;
import com.ystu.soap.services.chat.db.entity.Userdata;
import javafx.util.converter.LongStringConverter;
import org.codehaus.jettison.json.JSONException;

import javax.annotation.Generated;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/auth")
public class AuthService {

    private DBInterface dbi = new DBInterface();

    @GET
    @Path("/right")
    @Produces(MediaType.TEXT_PLAIN)
    public synchronized Response getRight(
            @CookieParam("SESSIONID") Long sessionid)
    {
        Long uid = dbi.getRight(sessionid);
        if (uid!=null) return Response.status(200).entity(uid.toString()).build();
        else return Response.status(403).build();
    }

    @POST
    @Path("/login/{username}/{password}")
    public synchronized Response login(
            @DefaultValue("0") @PathParam("username") String username,
            @DefaultValue("0") @PathParam("password") String password
    )
    {
        Long sessionToken = dbi.login(username,password);
        if (sessionToken!=null) {
            NewCookie sid = new NewCookie("SESSIONID", sessionToken.toString(),"/","","session id",1000000,false);
            return Response.status(200).cookie(sid).build();
        }
        else return Response.status(403).build();
    }

    @DELETE
    @Path("/logout")
    public synchronized Response logout(
            @CookieParam("SESSIONID") Long sessionid)
    {
        if (dbi.logout(sessionid)!=null) {
            NewCookie sid = new NewCookie("SESSIONID", "","/","","session id",-1,false);
            return Response.status(200).cookie(sid).build();
        }
        else return Response.status(403).build();
    }

    @POST
    @Path("/create/{username}/{password}")
    public synchronized Response createUser(
            @DefaultValue("0") @PathParam("username") String username,
            @DefaultValue("0") @PathParam("password") String password)
    {
        Boolean check = dbi.createUser(username,password);
        if (check) return Response.status(200).build();
        else return Response.status(403).build();
    }

    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public synchronized List<User> userlist() throws JSONException
    {
        return dbi.getUserlist();
    }
}
