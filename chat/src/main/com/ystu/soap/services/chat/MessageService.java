package com.ystu.soap.services.chat;

import com.ystu.soap.services.chat.DBInterface;
import com.ystu.soap.services.chat.db.entity.Messagedata;
import org.codehaus.jettison.json.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Created by Some on 07.10.2017.
 */

@Path("/msg")
public class MessageService {

    private DBInterface dbi = new DBInterface();

    //Receive messages
    @GET
    @Path("/{msgid}")
    @Produces(MediaType.APPLICATION_JSON)
    public synchronized Messagedata getMessage(@PathParam("msgid") String msgID) throws JSONException
    {
        return dbi.getMessage(Long.getLong(msgID));
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public synchronized List<Messagedata> getAllMessages() throws JSONException
    {
        return dbi.getAllMessages();
    }

    //Send a message
    @POST
    @Path("/{text}")
    public synchronized Response sendMessage(
            @DefaultValue("0") @CookieParam("SESSIONID") Long sessionid,
            @DefaultValue("") @PathParam("text") String text)
    {
        try{
            if (dbi.addMessage(sessionid,text)){
                return Response.status(200).build();
            } else {
                System.out.println("no message added");
                return Response.status(403).build();
            }
        } catch(Exception e)
        {
            System.out.println("Error: "+e.getMessage());
            return Response.status(403).build();
        }
    }

    //Edit a message
    @PUT
    @Path("/{msgid}/{text}")
    public synchronized Response editMessage(
            @CookieParam("SESSIONID") Long sessionid,
            @PathParam("msgid") Long msgid,
            @PathParam("text") String text)
    {
        try{
            if (dbi.editMessage(sessionid,msgid, text)){
                return Response.status(200).build();
            } else {
                System.out.println("no message added");
                return Response.status(403).build();
            }
        } catch(Exception e)
        {
            System.out.println("Error: "+e.getMessage());
            return Response.status(403).build();
        }
    }

    //Delete a message
    @DELETE
    @Path("/{msgid}")
    public synchronized Response deleteMessage(
            @CookieParam("SESSIONID") Long sessionid,
            @PathParam("msgid") Long msgid)
    {
        try{
            if (dbi.deleteMessage(sessionid,msgid)){
                return Response.status(200).build();
            } else {
                System.out.println("no message added");
                return Response.status(403).build();
            }
        } catch(Exception e)
        {
            System.out.println("Error: "+e.getMessage());
            return Response.status(403).build();
        }
    }
}
