package com.ystu.soap.services.chat;

import com.sun.jersey.spi.resource.Singleton;
import com.ystu.soap.services.chat.data.User;
import com.ystu.soap.services.chat.db.*;
import com.ystu.soap.services.chat.db.entity.*;

import java.util.List;

@Singleton
public class DBInterface {

    private DAO dao = new DAO();

    //Files

    public String getFilename(Long fid)
    {
        return dao.getFilename(fid);
    }

    public Long saveFilename(String filename)
    {
        Filedata fd = new Filedata(filename);
        return dao.saveFilename(fd);
    }

    //Users

    public Long login(String username, String password)
    {
        return dao.login(username,password.hashCode());
    }
    
    public Boolean createUser(String username, String password) {
        return dao.createUser(new Userdata(username, password.hashCode()));
    }

    public List<User> getUserlist() {
        return dao.getUserlist();
    }

    public Userdata findUser(Long id) {
        return dao.findUser(id);
    }

    public Userdata findUser(String username) {
        return dao.findUser(username);
    }

    public Long getRight(Long sessionid) {
        return dao.getRight(sessionid);
    }

    public Boolean logout(Long sessionid)
    {
        return dao.logout(sessionid);
    }

    //Messages

    public List<Messagedata> getAllMessages()
    {
        return dao.getAllMessages();
    }

    public Messagedata getMessage(Long msgId)
    {
        return dao.getMessage(msgId);
    }

    public Boolean addMessage(Long sessionid, String text)
    {
        Sessiondata sd = dao.findSessionByToken(sessionid);
        if (sd!=null) return dao.addMessage(new Messagedata(sd.getUserId(),text));
        else return false;
    }

    public Boolean editMessage(Long sessionid, Long msgId, String message)
    {
        Sessiondata sd = dao.findSessionByToken(sessionid);
        if (sd!=null) return dao.editMessage(sd.getUserId(), msgId, message);
        else return false;
    }

    public Boolean deleteMessage(Long sessionid, Long msgId)
    {
        Sessiondata sd = dao.findSessionByToken(sessionid);
        if (sd!=null) return dao.deleteMessage(sd.getUserId(),msgId);
        else return false;
    }
}
