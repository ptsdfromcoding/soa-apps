autoupdate();
getRight();
$("#text").attr("disabled",true);
$("#send").attr("disabled",true);
$("#file").attr("disabled",true);
$("#upload").attr("disabled",true);

var msgArray = [];
var userArray = [];
var uid = 0;

///////////////////////////
//       Events
///////////////////////////

$("#send").on('click', function(e) {
    sendMessage();
});

$("#update").on('click', function(e) {
    getAllMessages();
});

$("#login").on('click', function(e) {
    login();
});

$('#register').on('click', function(e) {
    register();
});

$("#upload").on('click',function(e) {
    upload();
});

$('#file').on('change', function(e) {
    $('#filename').val($('#file').get(0).files[0].name);
});

///////////////////////////
//          Auth
///////////////////////////

function register() {
    var username = $("#username").val();
    var password = $("#password").val();
    $.ajax({
        type: "POST",
        url: "/api/auth/create/" + username + "/" + password,
        success: function() {
            alert("Registration successful!");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('There was a ' + textStatus + ": " + errorThrown);
            alert("Cannot create such user.");
        }
    });
}

function login() {
    var username = $("#username").val();
    var password = $("#password").val();
    $.ajax({
        type: "POST",
        url: "/api/auth/login/" + username + "/" + password,
        xhrFields: {withCredentials: true},
        success: function() {
            getRight();
            alert("Successfully logged in!");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('There was a ' + textStatus + ": " + errorThrown);
            alert("Your data are incorrect. Check your username and/or password.");
        }
    });
}

function logout() {
    $.ajax({
        type: "DELETE",
        url: "api/auth/logout",
        xhrFields: {withCredentials: true},
        success: function(){
            $("#logout").off('click');
            $("#logout").text("Login");
            $("#logout").attr('id','login');
            $("#login").on('click', function(e) {
                login();
            });
            $("#username").attr('disabled',false);
            $("#password").attr('disabled',false);
            $("#username").val("");
            $("#text").attr('disabled',true);
            $('#send').attr('disabled',true);
            $('#register').attr('disabled',false);
            $('#file').attr('disabled',true);
            $('#upload').attr('disabled',true);
            uid=0;
            getAllMessages();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('There was a ' + textStatus + ": " + errorThrown);
        }
    });
}

function getRight() {
    var sessionid = getCookie("SESSIONID");
    if (sessionid != "") {
        $.ajax({
            type: "GET",
            url: "api/auth/right",
            success: function(data){
                uid = parseInt(data);
                $("#login").off('click');
                $("#login").text("Logout");
                $("#login").attr('id','logout');
                $("#logout").on('click', function(e) {
                    logout();
                });
                $("#username").attr('disabled',true);
                $("#password").attr('disabled',true);
                $("#password").val("");
                $('#text').attr('disabled',false);
                $('#send').attr('disabled',false);
                $('#register').attr('disabled',true);
                $('#file').attr('disabled',false);
                $('#upload').attr('disabled',false);
                getAllMessages();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log('There was a ' + textStatus + ": " + errorThrown);
            }
        })
    }
}

function getCookie(cookieName) {
    var name = cookieName + "=";
    var CookieString = decodeURIComponent(document.cookie);
    var SplitCookies = CookieString.split(';');
    for (var i = 0; i < SplitCookies.length; i++) {
        var c = SplitCookies[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getUserlist() {
    $.ajax({
        type: "GET",
        url: "/api/auth/users",
        dataType: "json",
        success: function(data){userArray = data == null ? [] : (data instanceof Array ? data : [data])},
        error: function(jqXHR, textStatus, errorThrown) {
            alert('There was an error' + textStatus + " " + errorThrown);
        }
    });
}

///////////////////////////
//       Messages
///////////////////////////

function getAllMessages() {
    getUserlist();
    $.ajax({
        type: "GET",
        url: "/api/msg/all",
        dataType: "json",
        success: renderList,
        error: function(jqXHR, textStatus, errorThrown) {
            alert('There was an error' + textStatus + " " + errorThrown);
        }
    });
}

function autoupdate() {
    getUserlist();
    $.ajax({
        type: "GET",
        url: "/api/msg/all",
        dataType: "json",
        success: renderList,
        error: function(jqXHR, textStatus, errorThrown) {
            alert('There was an error' + textStatus + " " + errorThrown);
        },
        complete: function() {
            setTimeout(autoupdate,5000);
        }
    });
}

function renderList(data) {
    msgArray = data == null ? [] : (data instanceof Array ? data : [data]);
    $('#msgList div').remove();
    $.each(msgArray, function(id, message) {
        var uname = getUsername(message.userId);
        var html = '<div class="message" id="msgid' + message.id + '"><div style="display: inline-block"><b>' + uname + "</b>: " + message.text+"</div>";
        if (message.userId==uid && !~message.text.indexOf("http://")) {
            html+='<div class="message buttons" style="display: inline-block"><button class="btn btn-small glyph-button bedit"><span class="glyphicon glyphicon-pencil"></span></button>' +
                              '<button class="btn btn-small glyph-button bremove"><span class="glyphicon glyphicon-remove"></span></button>' +
                              '</div></div>';
        } else {
            html+='</div>';
        }
        $("#msgList").append(html);
    });
    events();
}

function events(){
    $(".bedit").on('click', function(e) {
        var msgid = $(this).parent().parent().attr("id").replace("msgid","");
        var text = $("#text").val();
        editMessage(msgid,text);
    });

    $(".bremove").on('click', function(e) {
        var msgid = $(this).parent().parent().attr("id").replace("msgid","");
        deleteMessage(msgid);
    });
}

function getUsername(msgid) {
    var uname = "";
    $.each (userArray, function(id, u) {
        if (u.id==msgid)
        {
            uname = u.username;
        }
    });
    return uname;
}

function sendMessage() {
    var text = $("#text").val();
    $.ajax({
        type: "POST",
        url: "/api/msg/" + text,
        success: function() {
            getAllMessages();
            $("#text").val("");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('There was a ' + textStatus + ": " + errorThrown);
            $("#text").val("");
        }
    });
}

function editMessage(msgid, text) {
    $.ajax({
        type: "PUT",
        url: "/api/msg/" + msgid + "/" + text,
        success: function() {
            alert("Successfully updated!");
            getAllMessages();
            $("#text").val("");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('There was a ' + textStatus + ": " + errorThrown);
        }
    });
}

function deleteMessage(msgid) {
    $.ajax({
        type: "DELETE",
        url: "/api/msg/" + msgid,
        success: function() {
            alert("Successfully deleted!");
            getAllMessages();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('There was a ' + textStatus + ": " + errorThrown);
        }
    });
}

///////////////////////////
//       Files
///////////////////////////

function upload(){
    var formData = new FormData();
    var obj = $('#file').get(0).files[0];
    formData.append('file',obj,obj.name);
    $.ajax({
        type: "POST",
        url: "/api/file/upload",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function() {
            alert("File uploaded!");
            $('#file').val("");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('There was a ' + textStatus + ": " + errorThrown);
        }
    });
}