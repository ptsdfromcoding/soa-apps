<html>
<head>
    <title>Simple chat</title>
    <link rel="stylesheet" href='webjars/bootstrap/3.3.7/css/bootstrap.css'>
    <link rel="stylesheet" href='resources/index.css'>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
            <div class="form-group">
                <div class="input-group col-sm-12">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="username" class="form-control" id="username" placeholder="Username"/>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" class="form-control" id="password" placeholder="Password"/>
                    <div class="input-group-btn"><button class="btn btn-default" type="button" id="login">Login</button></div>
                    <div class="input-group-btn"><button class="btn btn-default" type="button" id="register">Register</button></div>
                    </div>
                    </div>
                </div>
            </div>
          <div class="panel-body">
            <div class="container">
                <div id="msgList"></div>
            </div>
            <div class="panel-footer">
                 <div class="input-group col-sm-12">
                  <input type="text" class="form-control" id="text" placeholder="Message"/>
                  <div class="input-group-btn">
                    <button class="btn btn-default glyphicon glyphicon-share-alt" type="button" id="send"></button></div>
                  <div class="input-group-btn">
                    <button class="btn btn-default glyphicon glyphicon-refresh" type="button" id="update"></button></div>
                </div>
                <div class="input-group">
                    <label class="btn btn-file">
                        <input type="file" class="dropzone" id="file"/>
                    </label>
                    <div class="input-group-btn">
                        <button class="btn btn-default glyphicon glyphicon-share-alt" type="button" id="upload"></button></div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>

<script src='webjars/jquery/3.1.1/jquery.js'></script>
<script src='webjars/bootstrap/3.3.7/js/bootstrap.js'></script>
<script src='resources/dropzone/dropzone.js'></script>
<script src='resources/index.js'></script>
</body>
</html>
