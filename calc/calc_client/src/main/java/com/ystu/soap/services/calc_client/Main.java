package com.ystu.soap.services.calc_client;

import com.ystu.soap.services.calc_client.generated.CalcProviderService;

import java.util.Scanner;

/**
 * Created by Some on 30.09.2017.
 */
public class Main{

    public static void main(String[] args)
    {
        System.out.println(new CalcProviderService().getCalcProviderPort().sum(10,20));
        System.out.println(new CalcProviderService().getCalcProviderPort().subtract(30,40));
        System.out.println(new CalcProviderService().getCalcProviderPort().multiply(4,8));
        System.out.println(new CalcProviderService().getCalcProviderPort().divide(35,20));
    }
}
