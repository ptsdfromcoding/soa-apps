package com.ystu.soap.services.calc_server;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.*;

/**
 * Created by Some on 30.09.2017.
 */

@WebService
@SOAPBinding(style=SOAPBinding.Style.RPC)
public class CalcProvider {
    @WebMethod(operationName = "sum")
    public Double sum(@WebParam(name = "x") Double x, @WebParam(name = "y") Double y )
    {
        log(x, '+', y);
        return x+y;
    }

    @WebMethod(operationName = "subtract")
    public Double substract(@WebParam(name = "x") Double x, @WebParam(name = "y") Double y )
    {
        log(x, '-', y);
        return x-y;
    }

    @WebMethod(operationName = "multiply")
    public Double multiply(@WebParam(name = "x") Double x, @WebParam(name = "y") Double y )
    {
        log(x, '*', y);
        return x*y;
    }

    @WebMethod(operationName = "divide")
    public Double divide(@WebParam(name = "x") Double x, @WebParam(name = "y") Double y )
    {
        log(x, '/', y);
        if (y!=0) {return x/y;}
                else {return null;}
    }

    public void log(Double x, char op, Double y)
    {
        Double result=null;
        switch(op) {
            case ('+'): result = x+y; break;
            case ('-'): result = x-y; break;
            case ('*'): result = x*y; break;
            case ('/'): if (y!=0) {result = x/y;} break;

        }
        String stream = x.toString()+op+y.toString()+'='+result+"\n";

        try {
            FileWriter output = new FileWriter("log.txt", true);
            output.append(stream);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
